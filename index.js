const functions = require("firebase-functions");
const capacities = require('./capacities');


// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

exports.weeklyCapacity = functions.https.onRequest(async (request, response) => {
  response.send(await capacities.getWeeklyCapacity());
});
