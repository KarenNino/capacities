const Forecast = require('forecast-promise');
const forecast = new Forecast({
    accountId: '958918',
    token: '2733559.pt._MdjkiZmMKrKDaE767OORqyjOW4XDIomqxdYp1yvfNFM_thn_O86m0QmOZZVBAWzJ_D7Qb89FKb5EDERLY3QkA',
});

const ming_logo = '<svg viewBox="0 0 25 25" version="1.1" xmlns="http://www.w3.org/2000/svg" class="header-logo-icon" style="fill: #e00;width: 48px;"><g id="Symbols" stroke="none" stroke-width="1" fill-rule="evenodd"><g id="logo_symbol-VividRed"><path d="M6.6879835e-13,0.466516747 C6.6879835e-13,0.102039183 0.433967277,-0.0924942069 0.710795812,0.147704866 L11.2911041,9.32941437 C11.5135949,9.52261534 11.6413619,9.8014546 11.6413619,10.094466 L11.6413619,23.7896893 C11.6413619,24.1643417 11.3344274,24.4681336 10.9558992,24.4681336 L0.685462696,24.4681336 C0.306934555,24.4681336 6.6879835e-13,24.1643417 6.6879835e-13,23.7896893 L6.6879835e-13,0.466516747 Z M13.5331008,24.3618204 C13.2540541,24.6026212 12.8168029,24.4076988 12.8168029,24.0424924 L12.8168029,10.089397 C12.8168029,9.79579966 12.9455367,9.51640276 13.1698345,9.32281543 L23.8493194,0.106237753 C24.1283661,-0.134563044 24.565494,0.0604807463 24.565494,0.425687197 L24.565494,14.3762338 C24.565494,14.6698311 24.4368834,14.9491066 24.2125856,15.142694 L13.5331008,24.3618204 Z M24.565494,24.0345027 C24.565494,24.4065295 24.147801,24.6050921 23.8811608,24.359918 L20.5180546,21.2656296 C20.2183054,20.9899168 20.2183054,20.5003103 20.5180546,20.2245976 L23.8811608,17.1294437 C24.1476831,16.8842696 24.565494,17.0828322 24.565494,17.454859 L24.565494,24.0345027 Z" id="logo"></path></g></g></svg>';

const getCurrentWeekStartAndEnd = () => {
	const now = new Date();
	now.setHours(12, 0, 0, 0);
  
	const first = (now.getDate() - now.getDay()) + 1;
	const firstDay = new Date(now.setDate(first));
	const firstDayYear = firstDay.getFullYear();
	let firstDayMonth = firstDay.getMonth() + 1;
	if (firstDayMonth < 10) firstDayMonth = `0${firstDayMonth}`;
	let firstDayDate = firstDay.getDate();
	if (firstDayDate < 10) firstDayDate = `0${firstDayDate}`;
  
	const lastDay = new Date(now.setDate(now.getDate() + 6));
	const lastDayYear = lastDay.getFullYear();
	let lastDayMonth = lastDay.getMonth() + 1;
	if (lastDayMonth < 10) lastDayMonth = `0${lastDayMonth}`;
	let lastDayDate = lastDay.getDate();
	if (lastDayDate < 10) lastDayDate = `0${lastDayDate}`;
  
	return {
	  start: {
		day: firstDayDate,
		month: firstDayMonth,
		year: firstDayYear,
	  },
	  end: {
		day: lastDayDate,
		month: lastDayMonth,
		year: lastDayYear,
	  }
	}
  };


var options = {};
let startWeek;
let endWeek;


const peopleRoles = {
	'Business Design': {},
	'UX Design': {},
	'Copywriter': {},
	'PM': {}
}

let people = {}
let projects = {}
let personAssigments = {}

async function getRoles() {
	return forecast.roles().then(roles => {
		roles.forEach(({name, person_ids}) => {
		   if(peopleRoles[name]) {
			   peopleRoles[name] = { 
				   person_ids
			   }
		   }
	   })
   });
}

async function getAllPeople() {
	return forecast.people().then(peopleList => {
		peopleList.forEach(({
			id, 
			weekly_capacity,
			first_name,
			last_name
		}) => {
			people[id] = { 
				weekly_capacity,
				first_name,
				last_name
			}
		})
	});
}


async function getAllProjects() {
	return forecast.projects().then(projectList => {
		projectList.forEach(({id, name, code}) => {
			projects[id] = { name, code }
		})
	});
}

async function getPeople() {
	const roles = Object.keys(peopleRoles);
	const allPeople = await getAllPeople();
	for (const rol of roles) {
		peopleRoles[rol]
	}
}


function getDiffDays(firstDate, secondDate) {
	const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
	const diffDays = Math.round((firstDate - secondDate) / oneDay) + 1;
  	return diffDays;
}


function getWorkingDaysAssigment(startAssig, endAssig) {
	if(endAssig <= startWeek || startAssig >= endWeek) {
		console.log('Invalid date ranges')
	}

	let end = endAssig <= endWeek ? endAssig : endWeek;
  	let start = startAssig >= startWeek ? startAssig : startWeek;
  
  
	return getDiffDays(end, start);
}

function addPersonAssigment(assigment) {
	const {	person_id, 
			start_date: startDate, 
			end_date: endDate,
			allocation,
			project_id } = assigment;
 	
	console.log('personAssigments keys', Object.keys(personAssigments))

	let assigmentProjects = []

	if(personAssigments[person_id]) {
		assigmentProjects = personAssigments[person_id].assigmentProjects;
	}
		
	let days = getWorkingDaysAssigment(new Date(assigment.start_date), new Date(assigment.end_date))
	let load = people[person_id] ? 
		(allocation * days) / people[person_id].weekly_capacity : 0;

		load = Math.round(load * 100);

	let project_name = projects[project_id].name;
	let project_code = projects[project_id].code;

	assigmentProjects.push({
		project_id,
		project_name,
		project_code,
		days,
		allocation,
		load
	})

	personAssigments[person_id] = { assigmentProjects };
}

async function getAllAssigments() {
	return forecast.assignments(options).then(assignments => {
		for (const assigment of assignments) {
			addPersonAssigment(assigment);
		}
	});
}

function syncPeopleRolesProjects() {
	const keysPeopleRoles = Object.keys(peopleRoles);
	keysPeopleRoles.forEach((pr) => {

		let { person_ids, capacities } = peopleRoles[pr];
		capacities = capacities || [];
		

		person_ids.forEach(pId => {
			if(personAssigments[pId]) {
				capacities.push(
					{
						...people[pId],
						...personAssigments[pId]
					}
				)
			}
		});

		peopleRoles[pr].capacities = capacities;
	});
	
}

async function getWeeklyCapacity() {
	people = {}
	projects = {}
	personAssigments = {}

	let { start, end } = getCurrentWeekStartAndEnd();

	startWeek = new Date(`${start.year}-${start.month}-${start.day}`);
	endWeek = new Date(`${end.year}-${end.month}-${end.day - 2}`);
	
	options = {
		startDate: startWeek,
		endDate: endWeek,
	};

	
	await getRoles();
	await getAllProjects();
	await getAllPeople();
	await getAllAssigments();
	syncPeopleRolesProjects()
	
    return getWeeklyCapacityHTMLFormat()
}


function getWeeklyCapacityHTMLFormat() {
	const peopleRolesKeys = Object.keys(peopleRoles);
	let weekly_capacity_html = `
		${ming_logo}
		<h1> Weekly Capacity </h1>
		<h2> Date: ${startWeek.toISOString().split('T')[0]} / ${endWeek.toISOString().split('T')[0]} </h2>
		<ul style='list-style-type:none;'>
	`;
	
	let rolesList = '';
	
	peopleRolesKeys.forEach(pr => {
		let role = `<li><b>${pr.toUpperCase()}</b></br></br><ul style="list-style-type:none;">`;
		const capacities = peopleRoles[pr].capacities;
		let capacitiesList = '';
		let assigmentProjectList = '';
		capacities.forEach((c) => {
			assigmentProjectList = '';
			let capacityPerson = `<li>
				</br>
				<span><b>{status} ${c.first_name} ${c.last_name}</b><span>
				</br>
				<span><b>Current Capacity:</b> {current_capacity}</span>
				</br>
				<span><b>Projects:</b><span>
				</br>
				<ul>	
			`;
			let totalCapacity = 0;
			const projectsPerson = c.assigmentProjects.forEach((p) => {
				const assigProj = 
				assigmentProjectList += `<li><span>${p.load}% - ${p.project_name}${ p.project_code ? ' - (' + p.project_code + ')' : ''}</span></li>`;
				totalCapacity += p.load;
			})
			
			capacityPerson += assigmentProjectList + '</ul></br></li>';
			capacityPerson = capacityPerson.replace('{current_capacity}', `${totalCapacity}%`);
			capacityPerson = capacityPerson.replace('{status}', totalCapacity > 101 ? '🔴' : '🟢');
			capacitiesList += capacityPerson;
		});
		capacitiesList += '</ul>';
		role += capacitiesList + '</li>';
		rolesList += role;
	});
	weekly_capacity_html += rolesList;
	weekly_capacity_html += "</ul>"
	return weekly_capacity_html;
}


exports.getWeeklyCapacity = getWeeklyCapacity;